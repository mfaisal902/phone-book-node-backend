
const environment = {}

const dbConfig = {
    'MONGO_USER': 'test902',
    'MONGO_PASSWORD': 'test1234',
    'MONGO_DB': 'phone_book'
}

environment.production = {
    'httpPort' : 3000,
    'envName' : 'production',
    'mongoDbUrl':`mongodb+srv://${dbConfig.MONGO_USER}:${dbConfig.MONGO_PASSWORD}@cluster0.vcydr.mongodb.net/${dbConfig.MONGO_DB}?retryWrites=true&w=majority`
}

module.exports = environment.production
