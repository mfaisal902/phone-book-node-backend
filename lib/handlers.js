/*
 * Request Handlers
 *
 */


// Dependencies
const Contact = require('./../models/contact')

// Define all the handlers
const handlers = {};

// Not-Found
handlers.notFound = function(data,callback){
    callback(404);
};

// Users
handlers.phoneBook = function(data,callback){
    const acceptableMethods = ['post', 'get', 'put', 'delete','options'];
    if(acceptableMethods.indexOf(data.method) > -1){
        handlers._phoneBook[data.method](data,callback);
    } else {
        callback(405);
    }
};

// Container for all the users methods
handlers._phoneBook  = {};

handlers._phoneBook.options = function(data,callback){
    callback(200);
}

// Contact - post
// Required data: Name, email, phone
// Optional data: none
handlers._phoneBook.post = function(data,callback){

    const path = typeof(data.trimmedPath) == 'string' && data.trimmedPath.length > 0 ? data.trimmedPath.split("/")[1] : false
    console.log(data.trimmedPath," - Path : ", path);
    if(path === 'addContact'){

        // Check that all required fields are filled out
        const name = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
        const phone = typeof(data.payload.email) == 'string' && data.payload.email.trim().length > 0 ? data.payload.email.trim() : false;
        const email = typeof(data.payload.phoneNumber) == 'string' && data.payload.phoneNumber.trim().length > 0 ? data.payload.phoneNumber.trim() : false;

        if(name && phone && email){
            // add contact to mongodb
            const newContact = new Contact({
                name : data.payload.name,
                email : data.payload.email,
                phoneNumber : data.payload.phoneNumber
            })

            newContact.save()
                .then(r => {
                    callback(200,r)
                }).catch(e => {
                callback(503,{"Error" : "Error Occured while saving data to our database"})
            })

        } else {
            callback(400,{'Error' : 'Missing required fields'});
        }

    }else{
        callback(404,{'Error':'This Endpoint does not exists'})
    }
};

// Required data: none
// Optional data: none
handlers._phoneBook.get = function(data,callback){

    const path = typeof(data.trimmedPath) == 'string' && data.trimmedPath.length > 0 ? data.trimmedPath.split("/")[1] : false

    if(path === 'getAllContacts'){
        //get all contacts from mongodb
        Contact.find().
            then(r=>{
                callback(200,r)
            }).catch(e => {
            callback(503,{"Error" : "Error Occured while saving data to our database"})
            })
    }else{
        callback(404,{'Error':'This Endpoint does not exists'})
    }
};

// Required data: id
// Optional data: none
handlers._phoneBook.put = function(data,callback){

    const path = typeof(data.trimmedPath) == 'string' && data.trimmedPath.length > 0 ? data.trimmedPath.split("/")[1] : false

    if(path === 'updateContact'){
        // Check for required field
        const id = typeof(data.payload._id) == 'string' && data.payload._id.length > 0 ? data.payload._id.trim() : false;
        const name = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
        const email = typeof(data.payload.email) == 'string' && data.payload.email.trim().length > 0 ? data.payload.email.trim() : false;
        const phone = typeof(data.payload.phoneNumber) == 'string' && data.payload.phoneNumber.trim().length > 0 ? data.payload.phoneNumber.trim() : false;

        // Error if phone is invalid
        if(id && name && phone && email){
            Contact.findOneAndUpdate(id,{
                name : data.payload.name,
                email : data.payload.email,
                phoneNumber : data.payload.phoneNumber
            }).
                then(r =>{
                    callback(200,r)
                }).catch(e =>{
                    callback(500,{'Error' : 'Error While Updating Contact'})
                })
        } else {
            callback(400,{'Error' : 'Missing required field.'});
        }
    }else{
        callback(404,{'Error':'This Endpoint does not exists'})
    }
};

// Required data: id
// optional data : none
handlers._phoneBook.delete = function(data,callback){
    // Check that phone number is valid
    const path = typeof(data.trimmedPath) == 'string' && data.trimmedPath.length > 0 ? data.trimmedPath.split("/")[1] : false
    if(path === 'deleteContact'){
        // Check for required field
        const id = typeof (data.trimmedPath.split("/")[2]) === 'string' && data.trimmedPath.split("/")[2].length > 0 ? data.trimmedPath.split("/")[2].trim() : false;

        // Error if phone is invalid
        if(id){
            Contact.findByIdAndDelete(id).
            then(r =>{
                callback(200,r)
            }).catch(e =>{
                callback(500,{'Error' : 'Error While Deleting Contact'})
            })
        } else {
            callback(400,{'Error' : 'Missing required field.'});
        }
    }else{
        callback(404,{'Error':'This Endpoint does not exists'})
    }
};

// Export the handlers
module.exports = handlers;
