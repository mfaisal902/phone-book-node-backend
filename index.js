/*
 * Primary file for API
 *
 */

// Dependencies
const http = require('http');
const StringDecoder = require('string_decoder').StringDecoder;
const config = require('./lib/config');
const handlers = require('./lib/handlers');
const helpers = require('./lib/helpers');
const mongoose = require('mongoose')
const cors = require("cors");


// Instantiate the HTTP server
const httpServer = http.createServer((req,res)=>{

    // Parse the url
    const baseURL =  req.protocol + '://' + req.headers.host + '/';
    const parsedUrl = new URL(req.url,baseURL)

    // Get the path
    const trimmedPath = parsedUrl.pathname.replace(/^\/+|\/+$/g, '')

    // Get the query string as an object
    const queryStringObject = parsedUrl.query;

    // Get the HTTP method
    const method = req.method.toLowerCase();

    //Get the headers as an object
    const headers = req.headers;

    // Get the payload,if any
    const decoder = new StringDecoder('utf-8');

    let buffer = '';

    req.on('data', function(data) {
        buffer += decoder.write(data);
    });

    req.on('end', function() {
        buffer += decoder.end();


        // Check the router for a matching path for a handler. If one is not found, use the notFound handler instead.
        const chosenHandler = typeof(router[trimmedPath.split("/")[0]]) !== 'undefined' ? router[trimmedPath.split("/")[0]] : handlers.notFound;

        // Construct the data object to send to the handler
        const data = {
            'trimmedPath' : trimmedPath,
            'queryStringObject' : queryStringObject,
            'method' : method,
            'headers' : headers,
            'payload' : helpers.parseJsonToObject(buffer)
        };


        // Route the request to the handler specified in the router
        chosenHandler(data,(statusCode , payload) => {

            // Use the status code returned from the handler, or set the default status code to 200
            statusCode = typeof(statusCode) == 'number' ? statusCode : 200;

            // Use the payload returned from the handler, or set the default payload to an empty object
            payload = typeof(payload) == 'object'? payload : {};

            // Convert the payload to a string
            const payloadString = JSON.stringify(payload);

            // Return the response
            res.setHeader('Content-Type', 'application/json');
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Credentials", "true");
            res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
            res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
            console.log("Status  ", statusCode);
            res.writeHead(statusCode);
            res.end(payloadString);
        });

    });
});



//connect to mongodb
mongoose.connect(config.mongoDbUrl).then((res) =>{
    console.log("Connected to Database !!!")

    // Start the HTTP server
    httpServer.listen(config.httpPort,()=>{
        console.log('The HTTP server is running on port '+config.httpPort);
    });

}).catch((error)=>{
    console.log("Error Connecting to database !!!")
})


// Define the request router
const router = {
    'phone-book' : handlers.phoneBook,
};
